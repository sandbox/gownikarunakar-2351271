<?php

/**
 * @file
 * Admin page callbacks for the domain redirection module.
 */

/**
 * Form to configure setting the site slogan.
 */
function domain_redirection_user_redirect_settings($form, &$form_state) {
  $country_widget = variable_get('domain_redirection_settings', array());
  $form['domain_redirection_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Redirect Settings'),
    '#tree' => TRUE,
  );
  $form['domain_redirection_settings']['intro'] = array(
    '#markup' => t('Please select the country from which you want the user to be redirected to other domain.'),
  );

  $countries = array();
  if (module_exists('countries')) {
    $countries = countries_load_multiple(FALSE);
  }

  foreach ($countries as $country) {
    if (!isset($form['domain_redirection_settings'][$country->continent])) {
      $form['domain_redirection_settings'][$country->continent] = array(
        '#type' => 'fieldset',
        '#title' => t('Continent') . ' : ' . country_property($country, 'continent', ''),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['domain_redirection_settings'][$country->continent]['default'] = array(
        '#type' => 'textfield',
        '#maxlength' => 300,
        '#title' => NULL,
        '#default_value' => isset($country_widget[$country->continent]['default']) && !empty($country_widget[$country->continent]['default']) ? $country_widget[$country->continent]['default'] : '',
      );
    }

    $form['domain_redirection_settings'][$country->continent][$country->iso2] = array(
      '#type' => 'textfield',
      '#maxlength' => 300,
      '#title' => $country->name,
      '#default_value' => isset($country_widget[$country->continent][$country->iso2]) && !empty($country_widget[$country->continent][$country->iso2]) ? $country_widget[$country->continent][$country->iso2] : '',
    );
  }

  $form = system_settings_form($form);
  return $form;
}
