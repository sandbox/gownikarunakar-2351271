This module allows to redirect users based on their country of 
origin to another url either internal to Drupal or external urls.

REQUIREMENTS
------------
This module requires the following modules:
 * Countries (https://www.drupal.org/project/countries)
 * Domain Access(https://www.drupal.org/project/domain)
 * Geo IP(https://www.drupal.org/project/geoip)
 
 
Installation:
*Turn module on and go to the admin/config/regional/domain_redirection_settings.
*Type the URL you want to redirect to and select the countries 
that should redirect to this address.  

Author
------
Karunakar Gowni
gownikarunakar@gmail.com
